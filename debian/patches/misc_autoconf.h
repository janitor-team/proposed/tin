--- a/include/autoconf.hin
+++ b/include/autoconf.hin
@@ -998,4 +998,8 @@ Added missing headers after gettext upda
  */
 #	undef HAVE_LIB_GNUTLS
 
+/* debian-specific */
+#define XFACE_ABLE
+#define NNTP_SERVER_FILE "/etc/news/server"
+
 #endif /* !TIN_AUTOCONF_H */
